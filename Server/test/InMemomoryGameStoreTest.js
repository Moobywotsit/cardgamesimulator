const gameStore = require('../Persistence/Stores/InMemory/InMemoryGameStore'); //Kind of like using a interface? because anything could go here is this by design??
const Game = require('../../Client/Public/js/Intergration/Game');
const VideoQualityEnum = require('../../Client/Public/js/Intergration/VideoQualityEnum');
const FilterCriteria = require('../../Client/Public/js/Intergration/GameFilterCritera');
const assert = require('chai').assert;

describe('In memory gamestore', function() {
    gameStore.addGame(new Game("name",  2, 0, VideoQualityEnum.MEDIUM, "Test Descrtiption"));

	it("Test getting games via filters", function () {
        var filterCriteria = new FilterCriteria(true, false, false, VideoQualityEnum.MEDIUM);
        let games = gameStore.getGames(filterCriteria);
    
        assert( games.get("name") != null);
    });

    it("Test adding and getting games", function () {
        let games = gameStore.getGames(null);
        assert( games.get("name") != null, "game not found!");
    });
});
