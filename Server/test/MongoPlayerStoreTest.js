var mongoose = require("mongoose");
mongoose.connect("mongodb+srv://GameSimulatorServer:Password@game-gar5x.mongodb.net/GameSimulator?retryWrites=true", { useNewUrlParser : true})
const store = require('../Persistence/Stores/Mongo/MongoPlayerStore');

const username = "testUser";
const password = "testPassword";

describe("Player Store Test", function(){

    it("Test login", function () {
        store.addPlayer({username: username, password: password} , function(added){
            assert(addded === true);
            let loggedIn = store.login({username: username, password: password});
            assert(loggedIn == true);
            store.removePlayer(username);
        });
    });

    it("Test removing player", function () {
        store.addPlayer({username: username, password: password} , function(added){
            store.removePlayer(username);
            store.getPlayer(username, function(found){
                assert(found == true);
            });
        });
    });

    it("Test adding player", function () {
        store.addPlayer({username: username, password:password} , function(added){
            assert(added == true);
        });
        store.removePlayer(username);
    });

    it("Test getting player", function () {
        store.addPlayer({username: username, password:password} , function(added){
            assert(added == true);
            store.getPlayer(username, function(player){
                assert(player.username === username);
                store.removePlayer(username);
            });
        });
    });
});