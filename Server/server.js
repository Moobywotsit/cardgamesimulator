'use strict';

const socketIO = require('socket.io');
const express = require("express");
const path = require("path");
const expressApp = express();
const session = require('express-session');
const portNumber = "3001";

var mongoose = require("mongoose");
mongoose.connect("mongodb+srv://GameSimulatorServer:Password@game-gar5x.mongodb.net/GameSimulator?retryWrites=true", { useNewUrlParser : true})

expressApp.use(
    session({ secret: 'my secret', resave: false, saveUninitialized: false })
)

expressApp.use(express.static(path.join(__dirname, '/../Client/Public')))

require("./routes")(expressApp);
var server = expressApp.listen(portNumber);

var gameStore = require('./Persistence/Stores/InMemory/InMemoryGameStore');
var playerStore = require('./Persistence/Stores/Mongo/MongoPlayerStore');

var io = socketIO.listen(server);
require("./SignalingService/SignalingServiceServer")(io, gameStore, playerStore);
console.log("Server Running On Port " + portNumber);