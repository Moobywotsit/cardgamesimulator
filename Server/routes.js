module.exports = function(app) {

    var bodyParser = require('body-parser');
    var multer = require('multer'); // v1.0.5
    var path = require('path');

    app.use(bodyParser.json()); // for parsing application/json
    app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

    var gameController = require('./controllers/GameController');
    var loginsController = require('./controllers/LoginsController');
    var playerController = require('./controllers/PlayerController');

    app.post('/delete', playerController.removePlayer)
    app.post('/signup', loginsController.signup);
    app.post('/login', loginsController.login);
    app.post('/logout', loginsController.logout);
    app.post('/games/getAvailable', gameController.getAvailable); //This is post because you need the filter object

    app.get('/player/stats/:username', playerController.stats)

    app.get('/',function(req, res){
        res.sendFile(path.join(__dirname + '/../Client/html/login.html'));
    });

    app.get('/browser',loginsController.loggedIn, function(req, res){
        res.sendFile(path.join(__dirname + '/../Client/html/browser.html'));
    });

    app.get('/game',loginsController.loggedIn,function(req, res){
        res.sendFile(path.join(__dirname + '/../Client/html/Game.html'));
    });

    app.get('/signup', function(req, res){
        res.sendFile(path.join(__dirname + '/../Client/html/signup.html'));
    });
}