const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const playerSchema = new Schema({
    username: {
        type: String,
        required: true    
    },
    password: {
        type: String,
        required: true
    },
    socketId: {
        type: String,
        required: false
    },
    wins: {
        type: Number,
        required: true
    },
    losses: {
        type: Number,
        required: true
    },
    gameName: {
        type: String,
        required: false
    }
});

module.exports = mongoose.model("Players", playerSchema);