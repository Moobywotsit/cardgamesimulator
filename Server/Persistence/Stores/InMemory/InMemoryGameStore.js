
class MemoryGameStore {
    constructor() {
        this.games = new Map();
    }

    getGames(filterCriteria) {
        if (filterCriteria == null) {
            return this.games;
        }

        var matchingGames = new Map();
        this.games.forEach(function(value, key, map){
            if (filterCriteria.matchesCriteria(value)) {
                matchingGames.set(key, value);
            }
        });

        return matchingGames;
    }

    getGame(gameName) {
        return this.games.get(gameName);
    }

    /**
     * adds a game to the list
     * @param {Game} game the instance of game to add
     */
    addGame(game) {
        if (this.games.get(game.name) == null) {
            this.games.set(game.name, game);
            return true; //adding was successfull?
        } else { 
            //throw some error here because this game name already exists
            console.log("game Allready exists");
            return false; //this could work i supposed alitle dodge but for now is fine
        };
    }

    endGame(name) {
        this.games.delete(name);
    }
}

var memoryGameStore = new MemoryGameStore();
module.exports = memoryGameStore;