class MongoPlayerStore {
    
    constructor() {
        this.Players = require("../../PersistanceObjects/Mongoose/Players");
        this.bcrypt = require("bcryptjs");
    }

    /**
     * this will be called when a new player joins the site and will store them as a player
     * @param {String} handleSignedUpresult function that is called when you are signedup or not
     * @param {Player} player the object holding relevent information about the the player
     */
    async addPlayer(player) {
        var username = player.username;
        var password = player.password;

        let user = await this.Players.findOne({ username: username });

        if (!player) {
             //Player allready exists and will not be added again
            return false;
        }

        let hashedPassword = await this.bcrypt.hash(password, 12);

        let playerInstance = new this.Players({
            username: username,
            socketId: "",
            password: hashedPassword,
            wins: 0,
            losses: 0
        });

        playerInstance.save();
        return true;
    }

    /**
     * Will be called when a player is determined to have finished playing
     * @param {String} player player object 
     */
    removePlayer(username) {
        this.Players
        .findOne({ username: username })
        .remove().exec()
        .catch(err => {
            console.log(err)
        });
    }

    getPlayer(username, handleReturn) {
        this.Players
        .findOne({ username: username })
        .then(player => {
            if (player) {
                //Player allready exists and will not be added again
                handleReturn(player);
            }else{
                handleReturn(null);
            }
        })
        .catch(err => {
            console.log(err)
            handleSignedUpresult(false);
        });
    }

    /**
     * Logs in a user not only checking they exist and has the password but adds the logged in flag to the db obj
     * @param {*} player player object representing player
     * @param {*} handleLoggedInresult function called when logged in or not
     */
    async login(player) {
        const username = player.username;
        const password = player.password;
    
        let user = await this.Players.findOne({ username: username });

        if (!user) {
            return false;
        }
        
        let matched = await this.bcrypt.compare(password, user.password);

        if(matched == true){
            return true;
        }else{
            return false;
        }
    }

    async addplayerSocketId(username, socketId){
        let user = await this.Players.findOne({ username: username });
        if (!user) {
            console.log("user does not exist");
            return null;
        }else{
            user.socketId = socketId;
            user.save();
            return true;
        }
    }

    /**
     * take socket id get username
     * @param {String} socketId 
     */
    async getPlayerUsername(socketId){
        let player = await this.Players.findOne({ socketId: socketId });
        if(player){
            return player.username;
        }else{
            return null; //no name assigned to socketId
        }
    }

    /**
     * take socket id get username
     * @param {String} socketId 
     */
    getPlayerStats(username, handleStats){
        this.Players
            .findOne({ username: username })
            .then(user => {
                if(user){
                    handleStats(user.wins, user.losses);
                }else{
                    handleStats(null); //no user not found
                }
            })
            .catch(function(err){console.log(err)});
    }

    addWin(username){
        this.Players
            .findOne({ username: username })
            .then(player => {
                if (!player) {
                    console.log("user does not exist")
                }else{
                    player.wins = player.wins + 1;
                    player.save();
                }
            })
            .catch(function(err){console.log(err)});
    }

    addLoss(username){
        this.Players
            .findOne({ username: username })
            .then(user => {
                if (!user) {
                    console.log("user does not exist")
                }
                user.losses = user.losses + 1;
                user.save();
            })
            .catch(function(err){console.log(err)});
    }

    addCurrentGame(socketId, gameName){
        this.Players
            .findOne({ socketId: socketId })
            .then(user => {
                if (!user) {
                    console.log("user does not exist")
                }else{
                    user.gameName = gameName;
                    user.save();
                }
            })
            .catch(function(err){console.log(err)});
    }

    async getCurrentGame(socketId){
        var player = await this.Players.findOne({ socketId: socketId });
        if (!player) {
            console.log("user does not exist")
        }else{
            return player.gameName;
        }   
    }
}

//Singleton
let mongoPlayerStore = new MongoPlayerStore();
module.exports = mongoPlayerStore;