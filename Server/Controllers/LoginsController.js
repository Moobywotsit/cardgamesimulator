const playerStore = require("../Persistence/Stores/Mongo/MongoPlayerStore")

exports.signup = async function (req, res) {
    let player = {username: req.body.username, password: req.body.password} //Standadise this

    let signedUp = await playerStore.addPlayer(player);

    if(signedUp === true){
        res.send("Signedup")
    }else{
        res.send("failedToSignUp");
    }    
};

exports.login = async function (req, res) {
    let player = {username: req.body.username, password: req.body.password} //Standadise this
    
    let loggedIn = await playerStore.login(player)
    if(loggedIn === true){
        req.session.isLoggedIn = true;
        res.send("loggedIn")
    }else{
        res.send("failedToLogIn");
    }
};

exports.loggedIn = function(req, res, next) {
    if(req.session.isLoggedIn == true){
        next(); //call next thing
    }else{
        res.redirect("/"); //go back to login if you are not logged in
    }

}

exports.logout = function(req, res) {
    let player = {username: req.body.username} //Standadise this
    req.session.isLoggedIn = false;
    res.send("loggedOut");
}