const gameStore = require('../Persistence/Stores/InMemory/InMemoryGameStore'); //Kind of like using a interface? because anything could go here is this by design??
const GameFilterCriteria = require("../../Client/Public/js/Intergration/GameFilterCritera")
/**
 * TODO: Might need to add the filter criteria as a param to this method rather than pulling it out of the req object makes it abit cleaner.
 * Gets a list of games that are available using the filter criteria object 
 * @param {string} req - the request object
 * @param {string} res - the object used for responding
 */
exports.getAvailable = function (req, res) {
    let filter;
    //var body = JSON.parse(req.body);
    if(req.body.full != null && req.body.friend != null && req.body.spectators != null && req.body.minVideoQuality != null){    
        filter = new GameFilterCriteria(req.body.full, req.body.friend, req.body.spectators, req.body.minVideoQuality); //This will never crash if you dont send the right thing. do we want this? 
    }

    let instancesRes = _strMapToObj(gameStore.getGames(filter));//Not sure if this is the way i wanna do this.
    
    res.send(instancesRes);//This could be a problem here 
};

//This is used to unmarshal the maps returned by the gameStore
function _strMapToObj(strMap) {
    let obj = Object.create(null);
    for (let [k,v] of strMap) {
        obj[k] = v;
    }
    return obj;
}