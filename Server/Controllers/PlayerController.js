const playerStore = require("../Persistence/Stores/Mongo/MongoPlayerStore")

exports.stats = function (req, res) {
    let username = req.params.username;

    playerStore.getPlayerStats(username, function(wins, losses){
        res.send({wins: wins, losses: losses});
    });
};

exports.removePlayer  = function(req, res) {
    let username = req.body.username;

    playerStore.removePlayer(username); //Fire and forget
    res.send("deleted");
}