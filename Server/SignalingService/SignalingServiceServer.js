'use strict'

module.exports = function(io, gameStore, playerStore){
	let os = require('os');
	let messageConverter = require("../../Client/Public/js/Intergration/MessageConverter");

	io.sockets.on('connection', async function (socket) {
		
		socket.on('register user with socket', async function(username){
			let added = await playerStore.addplayerSocketId(username,socket.id);
			console.log(socket.id + username + added);
			socket.emit('registered');
		});

		socket.on('chat message', function(message){
			for (let room in socket.rooms){
				if(socket.rooms[room] != socket.id){ //Pick the first room that is not the room that is your ID This means each player can only be in one room which is fine
					playerStore.getPlayerUsername(socket.id, function(username){
						socket.to(socket.rooms[room]).emit('chat message', username + ";" + message);
					});
				}
			}
		});

		socket.on('message', function (message) {
			console.log('Client said: ', message);
			for (let room in socket.rooms){
				if(socket.rooms[room] != socket.id){ //Pick the first room that is not the room that is your ID This means each player can only be in one room which is fine
					socket.to(socket.rooms[room]).emit('message', message);
				}
			}
		});
	
		/**
		* message format is: 'room name;maxNumPlayers;spectators;minVideoQuality;description' or 'my game;2;false;high;this is my game
		*/
		socket.on("create", function (roomMessage) {
			let roomName = messageConverter.getRoomNameFromMessage(roomMessage);
			console.log('Received request create room called ' + roomName);

			let clientsInRoom = io.sockets.adapter.rooms[roomName];
			let numClients = clientsInRoom ? Object.keys(clientsInRoom.sockets).length : 0;
			if (numClients === 0) {
				socket.join(roomName); 
				let game = messageConverter.createGameObjectFromRoomMessage(roomMessage);
				game.addPlayer();
				gameStore.addGame(game);
				playerStore.addCurrentGame(socket.id, roomName);
				console.log('Client ID ' + socket.id + ' created room ' + roomName);
				socket.emit('created', roomName); //This will tell all the clients to update there lists with all the joinable games.
			} else {
				//this room is taken.
				socket.emit('roomAlreadyExists', roomName); //This will cause the client to show an error message cause the room allready exists.
			}
		});
	
		/**
		* This will need a custom message with the clients video quality. (possibly more things also) to make sure they are eligable to join the game. Should only show games you are eligible for
		*/
		socket.on('join', async function (room) {
			let roomName = messageConverter.getRoomNameFromMessage(room);
			console.log('Received request to join room ' + roomName);
			let game = gameStore.getGame(roomName); 
			if (game != null) {
				let username = await playerStore.getPlayerUsername(socket.id);
				console.log(username + " is joining the game");
				let clientsInRoom = io.sockets.adapter.rooms[roomName];
				let numClients = clientsInRoom ? Object.keys(clientsInRoom.sockets).length : 0;
				
				console.log('Room ' + room + ' currently has ' + numClients + ' client(s)');
				if (numClients === 0) {
					console.log("re-created room that was empty");
					socket.join(roomName); 	
					console.log('Client ID ' + username + ' created room ' + roomName);
					socket.emit('created', roomName); //This will tell all the clients to update there lists with all the joinable games.
					playerStore.addCurrentGame(socket.id, roomName);
				} else if (numClients < game.maxNumberOfPlayers) { //this will need to be like => max players as set buy the room settings. 
					//Join the room as the next player
					console.log('Client ID ' + socket.id + ' joined room ' + room);
					console.log("joining");
					game.addPlayer();
					socket.in(room).emit('join', username);
					socket.join(room); //joins the room
					socket.emit('joined', room); //tells the user who initiated the join that they have joined the room.
					socket.in(room).emit('ready'); //tell the people in the room that you have joined
					playerStore.addCurrentGame(socket.id, roomName);
				}
			}
		});

		socket.on('hostUsername', function(message){
			var message = message.split(";");
			socket.in(message[0]).emit("hostUsername", message[1]); //tell the people in the room that you have joined
		});

		socket.on("result", function(winOrLoseUsername){

			let winOrLose = winOrLoseUsername.split(";")[0];
			let username = winOrLoseUsername.split(";")[1];

			if(winOrLose === "lost"){
				playerStore.addLoss(username);			
			}else{
				playerStore.addWin(username);
			}
			
			for (let room in socket.rooms){
				if(socket.rooms[room] != socket.id){ //Pick the first room that is not the room that is your ID This means each player can only be in one room which is fine
					if(winOrLose === "lost"){
						socket.to(socket.rooms[room]).emit('won');
					}
					
				}
			}

		});

		socket.on("disconnect", async function() {
			playerLeft(socket.id);
		});

		socket.on('ipaddr', function () {
			let ifaces = os.networkInterfaces();
			for (let dev in ifaces) {
				ifaces[dev].forEach(function (details) {
					if (details.family === 'IPv4' && details.address !== '127.0.0.1') {
						socket.emit('ipaddr', details.address);
					}
				});
			}
		});
	
	});

	async function playerLeft(socketId){
		let gameName = await playerStore.getCurrentGame(socketId);
		let game = gameStore.getGame(gameName);
		if(game){
			if(game.removePlayer() === true){ //game is empty
				console.log("room Closing down: " + gameName);
				gameStore.endGame(gameName);
			}
		}
		io.to(gameName).emit("left");
	}
}