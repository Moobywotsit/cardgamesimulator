
const isOnServer = (! (typeof window != 'undefined' && window.document));

if(isOnServer){
    var Game = require("./Game") //This should be a const but because of scoping has to be var NOT NICE
}

/**
* This takes the message that would come from socket.IO which would be in the format: 'room name;maxNumPlayers;spectators;minVideoQuality' or 'my game,2,false,high
 * This will handle everything todo with building and deconstructing messages so there is only one place that this needs to be used.
 * This file and its functions will be avaiable on both the server and the client.
*/
function createGameObjectFromRoomMessage(message) {
    var messageComponents = _breakMessageIntoComponents(message);
    console.log(message);
    var gameObj = new Game(messageComponents[0], messageComponents[1], messageComponents[2], messageComponents[3], messageComponents[4]);
    console.log(gameObj);
    return gameObj;
}

function getRoomNameFromMessage(message) {
    var messageComponents = _breakMessageIntoComponents(message);
    return messageComponents[0];
}

function getMessageWithoutRoomName(message) { 
    var messageComponents = _breakMessageIntoComponents(message);
    return messageComponents[1];
}

function createMessageFromGameObjectAndName(game, name){
    return name + ';' + game.maxNumberOfPlayers + ';' + game.spectators + ';' + game.minVideoQuality + ';' + game.description;
}

function _breakMessageIntoComponents(message) {
    return message.split(";");
}

if(isOnServer){    
    exports.createGameObjectFromRoomMessage = createGameObjectFromRoomMessage;
    exports.getRoomNameFromMessage = getRoomNameFromMessage;
    exports.getMessageWithoutRoomName = getMessageWithoutRoomName;
    exports.createMessageFromGameObjectAndName = createMessageFromGameObjectAndName;
}