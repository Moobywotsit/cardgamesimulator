const isOnServer = ! (typeof window != 'undefined' && window.document);

if(isOnServer === true){
    var VideoQualityEnum = require("./VideoQualityEnum");
}

/**
 * TODO: fill out as i think of more critera to filter on.
 * object use to filter out games when using getAvailableGames
 * @param {boolean} full - is the game full or not
 * @param {boolean} friend - only show games that are hosted by friends
 */
class GameFilterCriteria {
    constructor(full, friend, spectators, minVideoQuality) {
        this.full = full === 'true' ? true : false;
        this.friend = friend === 'true' ? true : false;
        this.spectators = spectators === 'true' ? true : false;
        this.minVideoQuality = minVideoQuality;
    } 

    _fullMatches(gameObjectFull) {
        if(this.full != null && this.full == false){
            return gameObjectFull;
        }else{
            return true;
        }
    }

    _friendMatches(gameObjectFriend) {
        return true; //not implemented but left for future
    }

    _spectatorsMatches(gameObjectSpectators) {
        return true; //not implemented but left for future
    }

    _minVideoQualityMatches(gameObjectMinVideoQuality) {
        return this.minVideoQuality != null ? VideoQualityEnum.properties[gameObjectMinVideoQuality].isHigher(this.minVideoQuality) : true; //if null it will return it
    }

    matchesCriteria(gameObject) {
        return this._fullMatches(gameObject.full()) &&
            this._friendMatches(gameObject.friend) &&
            this._spectatorsMatches(gameObject.spectators) &&
            this._minVideoQualityMatches(gameObject.minVideoQuality);
    }
}

if(isOnServer === true){
    module.exports = GameFilterCriteria;
}