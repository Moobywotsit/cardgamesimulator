/**
* This is the object that is responsable for representing an available game.
*/
class Game {
    constructor(name, maxNumberOfPlayers, spectators, minVideoQuality, description) {
        this.name = name;
        this.maxNumberOfPlayers = maxNumberOfPlayers;
        this.spectators = spectators;
        this.minVideoQuality = minVideoQuality;
        this.currentPlayers = 1; //when this is created it will be created by a player
        this.description = description;
    }

    //This only serves as a count there is no way of matching this number to the real players, not sure if this is good, would be better to link into the sockets room game stats perhaps?
    addPlayer() {
        if (this.currentPlayers < this.maxNumberOfPlayers) {
            this.currentPlayers++;
            return true;
        } 
        return false;
    }

    removePlayer() {
        if (this.currentPlayers > 0) { 
            this.currentPlayers--;
        }
        if(this.currentPlayers <= 0){
            return true;
        }
        return false;
    }

    full() {
        return this.maxNumberOfPlayers - this.currentPlayers > 0 ? true : false; 
    }

    //Static method that returns game object takes String json object 
    static unMarshal(jsonGameObj){
        return new Game(jsonGameObj.name, jsonGameObj.maxNumberOfPlayers, jsonGameObj.spectators,
            jsonGameObj.minVideoQuality, jsonGameObj.description);
    }

}

if(! (typeof window != 'undefined' && window.document)){
    module.exports = Game;
}