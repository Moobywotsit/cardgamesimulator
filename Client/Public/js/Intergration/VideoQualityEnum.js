
var VideoQualityEnum = {
    LOW: 1,
    MEDIUM: 2,
    HIGH: 3,
    properties: {
      1: {name: "low", isHigher: function(videoQualityEnum) {
          return videoQualityEnum == 1 ? true : false;
      }},
      2: {name: "medium", isHigher: function(videoQualityEnum) {
        return videoQualityEnum <= 2 ? true : false;
      }},
      3: {name: "high", isHigher: function(videoQualityEnum) {
        return videoQualityEnum <= 3 ? true : false;
      }}
    }
  };

if(! (typeof window != 'undefined' && window.document)){    
  module.exports = VideoQualityEnum;
}