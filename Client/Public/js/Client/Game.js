'use strict';

//You can only get the this page via the browser. meaning the session memory will allwasy be filled

var isChannelReady = false;
var isInitiator = false;
var createGame = (window.sessionStorage.createGame === 'true'); //This is similar to isInitiator but that is also used to show that the room was create sucessfully
var isStarted = false;
var gameName = window.sessionStorage.gameName;
var localStream;
var pc;
var remoteStream;
var turnReady;
var remoteUsername;


var pcConfig = { //Check this
  'iceServers': [{
    'urls': 'stun:stun.l.google.com:19302'
  }]
};

// Set up audio and video regardless of what devices are present.
var sdpConstraints = { //Check this
  offerToReceiveAudio: true,
  offerToReceiveVideo: true
};

var socket = io.connect('');

socket.emit('register user with socket', window.sessionStorage.username);

socket.on("registered", function(){
  if(createGame === true){
    var createRoom = createMessageFromGameObjectAndName(JSON.parse(window.sessionStorage.gameObj), gameName);
    socket.emit("create", createRoom)
  }else{
    console.log("joining " + gameName);
    socket.emit("join", gameName);
  }
});

//called when you create a room
socket.on('created', function(room) {
  console.log('Created room ' + room);
  isInitiator = true;
  setupStream();
});

//This allready hooks into my idea for when the room is full
socket.on('full', function(room) {
  console.log('Room ' + room + ' is full');
});

//When in a room and someone joins it this is called
socket.on('join', function (username){
  console.log('Another peer made a request to join room by the name of: ' + username);
  console.log('you are the initiator of room ');

  remoteUsername = username;
  document.getElementById("remoteUsername").innerText = remoteUsername;
  socket.emit("hostUsername", gameName + ";" + window.sessionStorage.username);
  isChannelReady = true;
});

socket.on('hostUsername', function(username){
  remoteUsername = username;
  document.getElementById("remoteUsername").innerText = remoteUsername;
});

// when you manage to join a room this will be called
socket.on('joined', function(room) {
  console.log('joined: ' + room);
  isChannelReady = true;
  setupStream();
});

////////////////////////////////////////////////

//This can be used to send a message to everyone in the same room as you (in my server anyway)
function sendMessage(message) {
  console.log('Client sending message: ', message); 
  socket.emit('message', message);
}

function sendChatMessage(message){
  console.log('Client sending Chat Message: ', message); 
  socket.emit('chat message', message);
}

socket.on('chat message', function(message){
    var chatBox = document.getElementById("chatBox");
    addMessage(chatBox, message, "chatbox__body__message--left");
});

// This client receives a message
socket.on('message', function(message) {
  console.log('Client received message:', message);
  if (message === 'got user media') {
    maybeStart();
  } else if (message.type === 'offer') {
    if (!isInitiator && !isStarted) {
      maybeStart();
    }
    pc.setRemoteDescription(new RTCSessionDescription(message));
    doAnswer();
  } else if (message.type === 'answer' && isStarted) {
    pc.setRemoteDescription(new RTCSessionDescription(message));
  } else if (message.type === 'candidate' && isStarted) {
    var candidate = new RTCIceCandidate({
      sdpMLineIndex: message.label,
      candidate: message.candidate
    });
    pc.addIceCandidate(candidate);
  } else {
    if(message === "healthUp"){
      increaseHealthByOne(false);
    }else if(message === "healthDown"){
      decreaseHealthByOne(false);
    }
  }
});

socket.on("left", function(message){
  if (isStarted) {
    handleRemoteHangup();
  }

  document.getElementById("userLeft").style.display = "block";
});

////////////////////////////////////////////////////

var localVideo = document.getElementById('localVideo');
var remoteVideo = document.getElementById('remoteVideo');

function setupStream(){
  navigator.mediaDevices.getUserMedia({
    audio: false,
    video: { width: 1280, height: 720 }
  }).then(gotStream).catch(function(e) {
    alert('getUserMedia() error: ' + e.name);
  });
}

function gotStream(stream) {
  console.log('Adding local stream.');
  localStream = stream;
  localVideo.srcObject = stream;
  sendMessage('got user media');
  if (isInitiator) {
    maybeStart();
  }
}

var constraints = {
  video: true
};

console.log('Getting user media with constraints', constraints);

if (location.hostname !== 'localhost') {
  requestTurn(
    'https://computeengineondemand.appspot.com/turn?username=41784574&key=4080218913'
  );
}

function maybeStart() {
  console.log('>>>>>>> maybeStart() ', isStarted, localStream, isChannelReady);
  if (!isStarted && typeof localStream !== 'undefined' && isChannelReady) {
    console.log('>>>>>> creating peer connection');
    createPeerConnection();
    pc.addStream(localStream);
    isStarted = true;
    console.log('isInitiator', isInitiator);
    if (isInitiator) {
      doCall();
    }
  }
}

/////////////////////////////////////////////////////////

function createPeerConnection() {
  try {
    pc = new RTCPeerConnection(null);
    pc.onicecandidate = handleIceCandidate;
    pc.onaddstream = handleRemoteStreamAdded;
    pc.onremovestream = handleRemoteStreamRemoved;
    console.log('Created RTCPeerConnnection');
  } catch (e) {
    console.log('Failed to create PeerConnection, exception: ' + e.message);
    alert('Cannot create RTCPeerConnection object.');
    return;
  }
}

function handleIceCandidate(event) {
  console.log('icecandidate event: ', event);
  if (event.candidate) {
    sendMessage({
      type: 'candidate',
      label: event.candidate.sdpMLineIndex,
      id: event.candidate.sdpMid,
      candidate: event.candidate.candidate
    });
  } else {
    console.log('End of candidates.');
  }
}

function handleCreateOfferError(event) {
  console.log('createOffer() error: ', event);
}

function doCall() {
  console.log('Sending offer to peer');
  pc.createOffer(setLocalAndSendMessage, handleCreateOfferError);
}

function doAnswer() {
  console.log('Sending answer to peer.');
  pc.createAnswer().then(
    setLocalAndSendMessage,
    onCreateSessionDescriptionError
  );
}

function setLocalAndSendMessage(sessionDescription) {
  pc.setLocalDescription(sessionDescription);
  console.log('setLocalAndSendMessage sending message', sessionDescription);
  sendMessage(sessionDescription);
}

function onCreateSessionDescriptionError(error) {
  trace('Failed to create session description: ' + error.toString());
}

function requestTurn(turnURL) { //not sure i need this, in practice it provides other ways to host video i believe but not needed for this.
  var turnExists = false;
  for (var i in pcConfig.iceServers) {
    if (pcConfig.iceServers[i].urls.substr(0, 5) === 'turn:') {
      turnExists = true;
      turnReady = true;
      break;
    }
  }
  if (!turnExists) {
    console.log('Getting TURN server from ', turnURL);
    // No TURN server. Get one from computeengineondemand.appspot.com:
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      if (xhr.readyState === 4 && xhr.status === 200) {
        var turnServer = JSON.parse(xhr.responseText);
        console.log('Got TURN server: ', turnServer);
        pcConfig.iceServers.push({
          'urls': 'turn:' + turnServer.username + '@' + turnServer.turn,
          'credential': turnServer.password
        });
        turnReady = true;
      }
    };
    xhr.open('GET', turnURL, true);
    xhr.send();
  }
}

function handleRemoteStreamAdded(event) {
  console.log('Remote stream added.');
  remoteStream = event.stream;
  remoteVideo.srcObject = remoteStream;
}

function handleRemoteStreamRemoved(event) {
  console.log('Remote stream removed. Event: ', event);
}

function hangup() {
  console.log('Hanging up.');
  stop();
  sendMessage('bye');
}

function handleRemoteHangup() {
  console.log('Session terminated.');
  stop();
  isInitiator = false;
}

function stop() {
  isStarted = false;
  pc.close();
  pc = null;
}

function sendMessageToOthers(message) {
  var chatBox = document.getElementById("chatBox");
  addMessage(chatBox, message, "chatbox__body__message--right");
  chatBox.scrollTop = chatBox.scrollHeight; //scroll to bottom
  sendChatMessage(message);
}

function addMessage(chatBoxDiv, message, messageClass) {
  var div = document.createElement("div");
  div.classList.add("chatbox__body__message");
  div.classList.add(messageClass); //This class needs to be styled so it is a slightly different colour than the other one
  var messagePara = document.createElement("p");
  messagePara.innerText = message;
  div.appendChild(messagePara);
  chatBoxDiv.appendChild(div);
}

function logout(){
  $.post("localhost:3001/logout", function(){
    window.location.href = "/"
  });
}

function decreaseHealthByOne(local){
  let divId = "remoteUserHp";
  if(local){
    divId = "localUserHp";
  }
  var hp = Number(document.getElementById(divId).innerText);
  hp --;
  document.getElementById(divId).innerText = hp;
  if(local){
    sendMessage("healthDown");
  }
}

function increaseHealthByOne(local){
  let divId = "remoteUserHp";
  if(local){
    divId = "localUserHp";
  } 

  var hp = Number(document.getElementById(divId).innerText);
  hp ++;
  document.getElementById(divId).innerText = hp;
  
  if(local){
    sendMessage("healthUp");
  }
}

function lose(){
  console.log("this player has lost");
  console.log(remoteUsername +" has won");
  socket.emit("result", "lost"+";"+window.sessionStorage.username);
  console.log("win"+";"+window.sessionStorage.username);
  document.getElementById("gameLost").style.display = "block";
}

socket.on("won", function(){
  document.getElementById("gameWon").style.display = "block";
  socket.emit("result", "won"+";"+ window.sessionStorage.username);
});

//javascript this up also need to add the press enter to send chat message thing
(function($) {
  $(document).ready(function() {
      var $chatbox = $('.chatbox'),
          $chatboxTitle = $('.chatbox__title'),
          $chatboxTitleClose = $('.chatbox__title__close');
      $chatboxTitle.on('click', function() {
          $chatbox.toggleClass('chatbox--tray');
      });
      $chatboxTitleClose.on('click', function(e) {
          e.stopPropagation();
          $chatbox.addClass('chatbox--closed');
      });
      $chatbox.on('transitionend', function() {
          if ($chatbox.hasClass('chatbox--closed')) $chatbox.remove();
      });
  });
})(jQuery);

window.onbeforeunload = function () {
  socket.emit('leaving');
}

window.onload = function(){
  var input = document.getElementById("messageField");
  document.getElementById("navUsername").innerText = window.sessionStorage.username;
  document.getElementById("localUsername").innerText = window.sessionStorage.username;
  document.getElementById("gameNameLabel").innerHTML = gameName;

  //wins
  $.get("http://localhost:3001/player/stats/" + window.sessionStorage.username, function(data){
    document.getElementById("userWins").innerText = data.wins;
    document.getElementById("userLosses").innerText = data.losses;
  });

  input.addEventListener("keyup", function(event) {
    event.preventDefault();
    if (event.keyCode === 13) {
      var message = input.value;
      input.value = "";
      sendMessageToOthers(message);
    }
  });
}