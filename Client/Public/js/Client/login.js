

function login(){
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;

    var loginCreds = {username: username, password: password};

    $.post("http://localhost:3001/login", loginCreds, function(data){
        console.log(data);
        if(data === "loggedIn"){
            console.log("right")
            window.sessionStorage.username = username;
            window.location.href = "/browser";
        }else{
            document.getElementById("errorMessage").style.display = "block";
        }
    });
}