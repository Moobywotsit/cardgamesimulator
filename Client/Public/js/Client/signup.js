
//This is called when the login button is clicked, for now it just redirects to the correct page
function signup(){
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;
    var confirmPassword = document.getElementById("confirmPassword").value;

    var signupCreds = {username: username, password: password};

    if(password == confirmPassword){    
        $.post("http://localhost:3001/signup", signupCreds, function(data){
            //redicrect to the right place only if you sign in if not erros
            if(data === "Signedup"){
                window.location.href = "/";
            }else{
                document.getElementById("errorMessage").style.display = "block";
            }
        });
    }else{
        console.log("passwords do not match") //Move this into a nice html element
    }
}