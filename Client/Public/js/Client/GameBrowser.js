/**
 * This is called to add the ui of a game into the page.
 * TODO: This needs the rest of the options that can be viewed adding to it
 * @param {Game} game
 */
function addGameToList(gameName, game) {
    var gamesList = document.getElementById("games");

    //row div
    var row = document.createElement("div");
    row.classList.add("row");
    //end row div

    //column div
    var column = document.createElement("div");
    column.classList.add("col-md-12");
    //end column div

    //header
    var header = document.createElement("h3");
    header.innerText = gameName;
    //end header

    //description
    var description = document.createElement("p");
    description.innerHTML = game.description;
    //end description

    //enter Game Button
    var button = document.createElement("a");
    button.innerText = "Join Game";
    //button.href = "#"; //This will need to be changed to link to the right place
    button.classList.add("btn");
    button.classList.add("btn-dark");
    button.style.color = "white";
    button.onclick = joinGame.bind(null,gameName);

    var hr = document.createElement("hr");

    column.appendChild(header);
    column.appendChild(description);
    column.appendChild(button);
    column.appendChild(hr);
    row.appendChild(column);
    gamesList.appendChild(row);
}

function joinGame(gameName){
    window.sessionStorage.gameName = gameName;
    window.sessionStorage.createGame = false;
    window.location.href = 'Game';
}

function createGame(){
    console.log("creating Game");
    
    window.sessionStorage.createGame = true;
    window.sessionStorage.gameName = document.getElementById("gameName").value;

    var description = document.getElementById("gameDescription").value;
    var spectators = $("input[name='spectators']:checked").val();
    var friends = $("input[name='friends']:checked").val();
    var minVideoQuality = $("input[name='videoQuality']:checked").val();

    var game = new Game(window.sessionStorage.gameName, 2, spectators, minVideoQuality, description);
    window.sessionStorage.gameObj = JSON.stringify(game);
    window.location.href = 'Game';
}

function removeGamesFromList() {
    var games = document.getElementById("games");
    while (games.firstChild) {
        games.removeChild(games.firstChild);
    }
}

//This can be got from the toher object
function createGameMessage(roomName, maxNumPlayers, spectators, minVideoQuality) {
    return roomName + ";" + maxNumPlayers + ";" + spectators + ";" + minVideoQuality;
}

//This function will get all the games that match the selected criteria
function getGames(filterCriteria) {
    removeGamesFromList();
    $.post("http://localhost:3001/games/getAvailable", filterCriteria, function (data) {
        for(var key in data){
            var game = Game.unMarshal(data[key]);
            addGameToList(key, game);
        }
    });
}

function searchForGames() {
    //Get the filter Criteria
    var spectators = $("input[name='spectators']:checked").val();
    var full = $("input[name='full']:checked").val();
    var friends = $("input[name='friends']:checked").val();
    var minVideoQuality = $("input[name='videoQuality']:checked").val();

    var filterCriteria = new GameFilterCriteria(full, friends, spectators, minVideoQuality);
    getGames(filterCriteria);
}

function logout(){
    $.post("http://localhost:3001/logout", function(loggedOut){
        window.location.href = "/"
    });
}

window.onload = function () {
    //wins
    document.getElementById("navUsername").innerText = window.sessionStorage.username;
    $.get("http://localhost:3001/player/stats/" + window.sessionStorage.username, function(data){
        document.getElementById("userWins").innerText = data.wins;
        document.getElementById("userLosses").innerText = data.losses;
    });
    getGames(null); //Get All Games
}